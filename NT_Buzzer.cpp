/*
 * @file NT_Led.cpp
 * @author Nicolas Toupin
 * @date 2019-11-24
 * @brief Led function definitions.
 */

#include "Arduino.h"
#include "NT_Buzzer.h"

Buzzer_t::Buzzer_t()
{
}

void Buzzer_t::Configure(bool Startup_Sp)
{
	_Startup_Sp = Startup_Sp;
}

void Buzzer_t::Attach(int Pin)
{
	_Pin = Pin;
}

void Buzzer_t::Init()
{
	pinMode(_Pin, OUTPUT);

	Enable = TRUE;

	Value_Sp = _Startup_Sp;
	Set(Value_Sp);
}

void Buzzer_t::Deinit()
{
	Value_Sp = _Startup_Sp;
	Set(Value_Sp);

	Enable = FALSE;
}

void Buzzer_t::_Write()
{
	digitalWrite(_Pin, Value_Sp);
}

void Buzzer_t::Set(bool State)
{
	if (Enable == TRUE)
	{
		Value_Sp = State;
	}
	else if (Enable == FALSE)
	{
		Value_Sp = _Startup_Sp;
	}

	_Write();
}

void Buzzer_t::Sing_Mario_Theme()
{
	if (Enable == TRUE)
	{
		int size = sizeof(Melody) / sizeof(int);
		for (int This_Note = 0; This_Note < size; This_Note++)
		{
			// to calculate the note duration, take one second
			// divided by the note type.
			//e.g. quarter note = 1000 / 4, eighth note = 1000/8, etc.
			int Note_Duration = 1000 / Tempo[This_Note];

			Buzz(Melody[This_Note], Note_Duration);

			// to distinguish the notes, set a minimum time between them.
			// the note's duration + 30% seems to work well:
			int Pause_Between_Notes = Note_Duration * 1.30;
			delay(Pause_Between_Notes);

			// stop the tone playing:
			Buzz(0, Note_Duration);
		}
	}
	else if (Enable == FALSE)
	{
		Set(_Startup_Sp);
	}
}

void Buzzer_t::Buzz(long Frequency, long Length)
{
	long Delay_Value = 1000000 / Frequency / 2; // calculate the delay value between transitions

	//// 1 second's worth of microseconds, divided by the frequency, then split in half since
	//// there are two phases to each cycle
	long Num_Cycles = Frequency * Length / 1000; // calculate the number of cycles for proper timing

	//// multiply frequency, which is really cycles per second, by the number of seconds to
	//// get the total number of cycles to produce
	for (long i = 0; i < Num_Cycles; i++)
	{
		// for the calculated length of time...
		Set(TRUE); // write the buzzer pin high to push out the diaphram
		delayMicroseconds(Delay_Value); // wait for the calculated delay value
		Set(FALSE); // write the buzzer pin low to pull back the diaphram
		delayMicroseconds(Delay_Value); // wait again or the calculated delay value
	}
}