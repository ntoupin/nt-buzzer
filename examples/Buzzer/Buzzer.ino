/*
   @file Buzzer.ino
   @author Nicolas Toupin
   @date 2019-11-24
   @brief Simple example of buzzer utilisation.
*/

#include <NT_Buzzer.h>

Buzzer_t Buzzer1;

void setup()
{
  Buzzer1.Configure(FALSE);
  Buzzer1.Attach(13);
  Buzzer1.Init();
}

void loop()
{
  Buzzer1.Sing_Mario_Theme();
}
